import json
import logging.config
import jsonschema
import yaml
import os
from multiprocessing import Pool

data_file = "./data/MF_CSI_2017-Q1.json"
# data_file = "./data/MF_CSI_test.json"
schema_object_file = "./schema/MF_CSI_2017-Q1_schema.json"
schema_col_file = "./schema/MF_CSI_schema_col.json"

logger = logging.getLogger(__name__)


# def find_col_root(schema):
#     result = ''
#     if schema:
#         if type(schema) == list:
#             result = schema
#         elif type(schema) == dict:
#             if 'properties' in schema:
#                 if type(schema['properties'] == dict):
#                     for key, value in schema['properties'].items():
#                         if 'type' in value:
#                             if value['type'] == 'array':
#                                 result = schema[key]
#
#     return result


def find_max_columns(schema):
    return len(schema['definitions']['interview']['properties'])


def get_fields_data(data) -> set:
    result = set()
    for r in data:
        for k in r.keys():
            result.add(k)
    return result


def get_fields_schema(schema) -> set:
    return set(schema['definitions']['interview']['properties'].keys())


def compare_fields(fields1, fields2):
    return json.dumps(
        {
            'diffData1': sorted(list(fields1.difference(fields2))),
            'diffData2': sorted(list(fields2.difference(fields1)))
        },
        ensure_ascii=False,
        indent=2,
        sort_keys=True
    )


def compare_schema_data(schema, data):
    fields_schema = get_fields_schema(schema)
    fields_data = get_fields_data(data)
    return compare_fields(fields_schema, fields_data)


def compare_schema_schema(schema1, schema2):
    fields_schema1 = get_fields_schema(schema1)
    fields_schema2 = get_fields_schema(schema2)
    return compare_fields(fields_schema1, fields_schema2)


def compare():
    base_schema_file = 'MF_CSI_{year}-Q{quarter}_schema.json'
    base_data_file = 'MF_CSI_{year}-Q{quarter}.json'

    for year in [2017, 2018]:
        for quarter in [1, 2, 3, 4]:
            schema_file_path = os.path.join("schema", base_schema_file.format(year=year, quarter=quarter))
            data_file_path = os.path.join("data", base_data_file.format(year=year, quarter=quarter))
            if os.path.exists(schema_file_path) and os.path.exists(data_file_path):
                with open(schema_file_path, 'rt', encoding="utf-8") as f1:
                    json_schema = json.load(f1)
                with open(data_file_path, 'rt', encoding="utf-8") as f2:
                    json_data = json.load(f2)
                result_compare = compare_schema_data(json_schema, json_data)
                file_result = os.path.join("logs",
                                           "{}-Q{}_schema_vs_{}-Q{}_data.json".format(year, quarter, year, quarter))
                with open(file_result, 'wt', encoding="utf-8") as f3:
                    f3.write(result_compare)
                print("Completed {}".format(file_result))

    file_completed = set()

    for year1 in [2017, 2018]:
        for quarter1 in [1, 2, 3, 4]:
            for year2 in [2017, 2018]:
                for quarter2 in [1, 2, 3, 4]:
                    file_result1 = os.path.join("logs", "{}-Q{}_schema_vs_{}-Q{}_schema.json".format(
                        year1, quarter1, year2, quarter2))
                    file_result2 = os.path.join("logs", "{}-Q{}_schema_vs_{}-Q{}_schema.json".format(
                        year2, quarter2, year1, quarter1))

                    if (year1 * 4 + quarter1) != (year2 * 4 + quarter2) and \
                            file_result1 not in file_completed and \
                            file_result2 not in file_completed:
                        schema_file1_path = os.path.join("schema",
                                                         base_schema_file.format(year=year1, quarter=quarter1))
                        schema_file2_path = os.path.join("schema",
                                                         base_schema_file.format(year=year2, quarter=quarter2))
                        if os.path.exists(schema_file1_path) and os.path.exists(schema_file2_path):
                            with open(schema_file1_path, 'rt', encoding="utf-8") as f1:
                                json_schema1 = json.load(f1)
                            with open(schema_file2_path, 'rt', encoding="utf-8") as f2:
                                json_schema2 = json.load(f2)
                            result_compare = compare_schema_schema(json_schema1, json_schema2)
                            with open(file_result1, 'wt', encoding="utf-8") as f3:
                                f3.write(result_compare)
                            file_completed.add(file_result1)
                            file_completed.add(file_result2)
                            print("Completed {}".format(file_result1))


def test_file(schema_file, json_file):
    with open(schema_file, 'rt', encoding="utf-8") as f2:
        json_schema = json.load(f2)
    with open(json_file, 'rt', encoding="utf-8") as f1:
        json_data = json.load(f1)

    logger.info("file {}, all total rows {}".format(json_file, len(json_data)))
    logger.info("file {}, columns in schema {}".format(schema_file, find_max_columns(json_schema)))

    for i, r in enumerate(json_data):
        try:
            jsonschema.validate(r, json_schema)
        except jsonschema.ValidationError as ex:
            logger.error("File {}, Validation error, row index {}, \nrow {}, \nerror\n{}".
                         format(json_file, i, json.dumps(r, indent=2, sort_keys=True, ensure_ascii=False), ex))


def test(v):
    test_file(v[0], v[1])


def test_schema():
    files = [
        (
            os.path.join("./schema", v.split('.')[0] + "_schema.json"),
            os.path.join("./data", v)
        ) for v in sorted(os.listdir("./data"))]

    with Pool(processes=7) as pool:
        pool.map(test, files)


if __name__ == "__main__":
    with open("./conf/log.yaml", mode='rt') as f:
        conf_log = yaml.safe_load(f)
    logging.config.dictConfig(conf_log)
    compare()
    # test_schema()
